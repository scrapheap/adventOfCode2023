package Day_17;

use v5.36;
use feature 'multidimensional';

use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadCharacterMap maxXY );

use List::Util qw( min );

use constant EW => 1;
use constant NS => 2;

sub part1 {
    my ( $file ) = @_;

    my $map = [ loadCharacterMap( $file ) ];

    my ( $maxX, $maxY ) = map { $_ - 1 } maxXY( @$map );

    my $worstCost = $maxX * $maxY * 100;

    my $graph = buildGraph( $map );

    my @costs;

    foreach my $from ( EW, NS ) {
        my $unvisited = { "$from,0,0" => 1 };
        my $toDo = {};
        my $visited = {};

        my $distances = {};
        $distances->{ "$from,0,0" } = 0;

        while( my $node = nextNode( $unvisited, $distances ) ) {
            delete $unvisited->{ $node };
            $visited->{ $node } = 1;

            foreach my $edge ( keys %{ $graph->{ $node } } ) {
                $distances->{ $edge } = min(
                    grep { defined $_ } (
                        $distances->{ $edge },
                        $distances->{ $node } + $graph->{ $node }->{ $edge }
                    )
                );

                if ( !$visited->{ $edge } ) {
                    $unvisited->{ $edge } = 1;
                }
            }
        }

        push @costs, $distances->{ "1,$maxX,$maxY" }, $distances->{ "2,$maxX,$maxY" };
    }

    return min( @costs );
}


sub part2 {
    my ( $file ) = @_;

    my $map = [ loadCharacterMap( $file ) ];

    my ( $maxX, $maxY ) = map { $_ - 1 } maxXY( @$map );

    my $worstCost = $maxX * $maxY * 100;

    my $graph = buildGraph2( $map );

    my @costs;

    foreach my $from ( EW, NS ) {
        my $unvisited = { "$from,0,0" => 1 };
        my $toDo = {};
        my $visited = {};

        my $distances = {};
        $distances->{ "$from,0,0" } = 0;

        while( my $node = nextNode( $unvisited, $distances ) ) {
            delete $unvisited->{ $node };
            $visited->{ $node } = 1;

            foreach my $edge ( keys %{ $graph->{ $node } } ) {
                $distances->{ $edge } = min(
                    grep { defined $_ } (
                        $distances->{ $edge },
                        $distances->{ $node } + $graph->{ $node }->{ $edge }
                    )
                );

                if ( !$visited->{ $edge } ) {
                    $unvisited->{ $edge } = 1;
                }
            }
        }

        push @costs, $distances->{ "1,$maxX,$maxY" }, $distances->{ "2,$maxX,$maxY" };
    }

    return min( @costs );
}

sub buildGraph {
    my ( $map ) = @_;

    my ( $maxX, $maxY ) = maxXY( @$map );

    my %graph;

    my %routes = (
        EW, [ [ -1,  0 ], [  1, 0 ] ],
        NS, [ [  0, -1 ], [  0, 1 ] ],
    );

    foreach my $from ( EW, NS ) {
        my $to = $from == NS ? EW : NS;

        for( my $y = 0; $y < $maxY; $y++ ) {
            for( my $x = 0; $x < $maxX; $x++ ) {
                my $edges = {};

                foreach my $route ( @{ $routes{ $from } } ) {
                    my $routeTotal = 0;

                    for my $dist ( 1..3 ) {
                        my $_x = $x + ( $route->[0] * $dist );
                        my $_y = $y + ( $route->[1] * $dist );

                        if ( ( 0 <= $_x < $maxX )  &&  ( 0  <= $_y < $maxY ) ) {
                            $routeTotal += $map->[ $_y ]->[ $_x ];

                            $edges->{ "$to,$_x,$_y" } = $routeTotal;
                        }
                    }
                }
            
                $graph{ "$from,$x,$y" } = $edges;
            }
        }
    }

    return \%graph;
}


sub buildGraph2 {
    my ( $map ) = @_;

    my ( $maxX, $maxY ) = maxXY( @$map );

    my %graph;

    my %routes = (
        EW, [ [ -1,  0 ], [  1, 0 ] ],
        NS, [ [  0, -1 ], [  0, 1 ] ],
    );

    foreach my $from ( EW, NS ) {
        my $to = $from == NS ? EW : NS;

        for( my $y = 0; $y < $maxY; $y++ ) {
            for( my $x = 0; $x < $maxX; $x++ ) {
                my $edges = {};

                foreach my $route ( @{ $routes{ $from } } ) {
                    my $routeTotal = 0;

                    for my $dist ( 1..3 ) {
                        my $_x = $x + ( $route->[0] * $dist );
                        my $_y = $y + ( $route->[1] * $dist );

                        if ( ( 0 <= $_x < $maxX )  &&  ( 0  <= $_y < $maxY ) ) {
                            $routeTotal += $map->[ $_y ]->[ $_x ];
                        }
                    }

                    for my $dist ( 4..10 ) {
                        my $_x = $x + ( $route->[0] * $dist );
                        my $_y = $y + ( $route->[1] * $dist );

                        if ( ( 0 <= $_x < $maxX )  &&  ( 0  <= $_y < $maxY ) ) {
                            $routeTotal += $map->[ $_y ]->[ $_x ];

                            $edges->{ "$to,$_x,$_y" } = $routeTotal;
                        }
                    }
                }
            
                $graph{ "$from,$x,$y" } = $edges;
            }
        }
    }

    return \%graph;
}

sub nextNode {
    my ( $unvisited, $distances ) = @_;

    my $minNode;

    foreach my $possible ( grep { exists $distances->{ $_ } } keys %$unvisited ) {
        if (
            ! defined $minNode
            || $distances->{ $minNode } > $distances->{ $possible }
        ) {
            $minNode = $possible;
        }
    }

    return $minNode;
}

1;
