package Day_23;

use v5.36;
use feature 'multidimensional';

no warnings 'recursion';

use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadCharacterMap  maxXY  dumpMap );
use List::Util qw( max );

use constant NORTH => 1;
use constant EAST  => 2;
use constant SOUTH => 4;
use constant WEST  => 8;

my %slope = (
    "^" => NORTH,
    ">" => EAST,
    "v" => SOUTH,
    "<" => WEST,
    "." => NORTH | EAST | SOUTH | WEST,
);

sub part1 {
    my ( $file ) = @_;

    my $map = [ loadCharacterMap( $file ) ];

    my ( $maxX, $maxY ) = map { $_ - 1 } maxXY( $map );

    my $x;
    my $y = 0;
    my $destX;
    my $destY = $maxY;
    for my $px ( 0 .. $maxX ) {
        if ( $map->[0]->[ $px ] eq "." ) {
            $x = $px;
        }
        if ( $map->[$maxY]->[ $px ] eq "." ) {
            $destX = $px;
        }
    }

    return walkMap( $x, $y, $destX, $destY,$map, 0 );
}


sub walkMap {
    my ( $x, $y, $destX, $destY, $map, $steps ) = @_;

    if ( $destX == $x && $destY == $y ) {
        return $steps;
    }

    my $current = $map->[$y]->[$x];
    $map->[$y]->[$x] = "#";

    my $mostSteps = 0;

    my $canTry = $slope{ $current };

    if( $canTry & NORTH
        && $y > 0
        && $map->[$y - 1]->[$x] ne "#"
    ) {
        $mostSteps = max( $mostSteps, walkMap( $x, $y - 1, $destX, $destY, $map, $steps + 1 ) );
    }

    if( $canTry & EAST
        && defined $map->[$y]->[$x + 1]
        && $map->[$y]->[$x + 1] ne "#"
    ) {
        $mostSteps = max( $mostSteps, walkMap( $x + 1, $y, $destX, $destY, $map, $steps + 1 ) );
    }

    if( $canTry & SOUTH
        && defined $map->[$y + 1]
        && $map->[$y + 1]->[$x] ne "#"
    ) {
        $mostSteps = max( $mostSteps, walkMap( $x, $y + 1, $destX, $destY, $map, $steps + 1 ) );
    }

    if( $canTry & WEST
        && $x > 0
        && $map->[$y]->[$x - 1] ne "#"
    ) {
        $mostSteps = max( $mostSteps, walkMap( $x - 1, $y, $destX, $destY, $map, $steps + 1 ) );
    }

    $map->[$y]->[$x] = $current;

    return $mostSteps;
}

sub part2 {
    my ( $file ) = @_;

    my $map = [ loadCharacterMap( $file ) ];

    my ( $maxX, $maxY ) = map { $_ - 1 } maxXY( $map );

    my $startX;
    my $startY = 0;
    my $destX;
    my $destY = $maxY;
    for my $px ( 0 .. $maxX ) {
        if ( $map->[0]->[ $px ] eq "." ) {
            $startX = $px;
        }
        if ( $map->[$maxY]->[ $px ] eq "." ) {
            $destX = $px;
        }
    }

    my $wormhole = {
        "$startX,$startY" => { x => $startX, y => $startY },
        "$destX,$destY" => { x => $destX, y => $destY },
    };

    for( my $y = 1; $y < $maxY; $y++ ) {
        for( my $x = 1; $x < $maxX; $x++ ) {
            next if $map->[$y]->[$x] eq "#";
            my $exits = 0;
            if( $map->[$y - 1]->[$x] ne "#") { $exits++ };
            if( $map->[$y]->[$x + 1] ne "#") { $exits++ };
            if( $map->[$y + 1]->[$x] ne "#") { $exits++ };
            if( $map->[$y]->[$x - 1] ne "#") { $exits++ };
            if ( $exits > 2 ) {
                $wormhole->{ "$x,$y" } = { x => $x, y => $y };
            }
        }
    }

    foreach my $route ( values %$wormhole ) {
        my ( $x, $y ) = ( $route->{x}, $route->{y} );

        # find distance and destination of each wormhole
        $map->[$y]->[$x] = "#";

        # walk north
        if ( $y > 0 && $map->[$y - 1][$x] ne "#" ) {
            my ( $routeDest, $dist ) = mapRoute( $map, $x, $y - 1, $wormhole, 0 );
            push @{ $route->{dests} }, [ $routeDest, $dist ];
        }

        # walk east
        if ( $x < $maxX && $map->[$y][$x + 1] ne "#" ) {
            my ( $routeDest, $dist ) = mapRoute( $map, $x + 1, $y, $wormhole, 0 );
            push @{ $route->{dests} }, [ $routeDest, $dist ];
        }

        # walk south
        if ( $y < $maxY && $map->[$y + 1][$x] ne "#" ) {
            my ( $routeDest, $dist ) = mapRoute( $map, $x, $y + 1, $wormhole, 0 );
            push @{ $route->{dests} }, [ $routeDest, $dist ];
        }

        # walk west
        if ( $x > 0 && $map->[$y][$x - 1] ne "#" ) {
            my ( $routeDest, $dist ) = mapRoute( $map, $x - 1, $y, $wormhole, 0 );
            push @{ $route->{dests} }, [ $routeDest, $dist ];
        }

        $map->[$y]->[$x] = ".";
    }


    # Do a DFS of wormholes;
    return traverseWormhole( "$startX,$startY", "$destX,$destY", $wormhole, 0, {} );
}


sub mapRoute {
    my ( $map, $x, $y, $wormhole, $steps ) = @_;

    $steps++;

    if ( exists $wormhole->{"$x,$y" } ) {
        return ("$x,$y", $steps);
    }

    my $current = $map->[$y]->[$x];
    $map->[$y]->[$x] = "#";

    my $dest;
    if( $y > 0  &&  $map->[$y - 1]->[$x] ne "#" ) {
        ( $dest, $steps ) = mapRoute( $map, $x, $y - 1, $wormhole, $steps);
    }

    if( defined $map->[$y]->[$x + 1]  &&  $map->[$y]->[$x + 1] ne "#" ) {
        ( $dest, $steps ) = mapRoute( $map, $x + 1, $y, $wormhole, $steps);
    }

    if( defined $map->[$y + 1]  &&  $map->[$y + 1]->[$x] ne "#" ) {
        ( $dest, $steps ) = mapRoute( $map, $x, $y + 1, $wormhole, $steps);
    }

    if( $x > 0 && $map->[$y]->[$x - 1] ne "#" ) {
        ( $dest, $steps ) = mapRoute( $map, $x - 1, $y, $wormhole, $steps);
    }

    $map->[$y]->[$x] = ".";

    return ( $dest, $steps );
}

sub traverseWormhole {
    my ( $current, $target, $wormholes, $distance, $seen ) = @_;

    return $distance if $current eq $target;

    $seen->{ $current } = 1;
    my $maxDist = 0;

    foreach my $dest ( @{ $wormholes->{$current}->{dests} } ) {
        if ( ! $seen->{$dest->[0]} ) {
            my $possible = traverseWormhole( $dest->[0], $target, $wormholes, $distance + $dest->[1], $seen );
            $maxDist = $maxDist < $possible ? $possible : $maxDist;
        }
    }

    delete $seen->{ $current };

    return $maxDist;
}


1;
