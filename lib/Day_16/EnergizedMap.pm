package Day_16::EnergizedMap;

use Modern::Perl;
use List::Util qw( sum0 );

sub new {
    my ( $class, $map ) = @_;

    my $self = {
        eMap => [ map { [ map { 0 } @$_ ] } @$map ],
    };

    return bless $self, $class;
}

sub energize {
    my ( $self, $x, $y ) = @_;

    $self->{eMap}->[$y]->[$x]++;

    return;
}

sub energizedCells {
    my ( $self ) = @_;

    my $total = 0;

    foreach my $row ( @{ $self->{eMap} } ) {
        foreach my $cell ( @$row ) {
            $total += $cell > 0 ? 1 : 0;            
        }
    }

    return $total;
}

1;
