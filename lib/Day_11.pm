package Day_11;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadCharacterMap );
use List::Util qw( none any sum0 );

our $expansion = 1000000;


sub part1 {
    my ( $file ) = @_;

    my @map = loadCharacterMap( $file );

    @map = expandRows( @map );
    @map = expandCols( @map );

    my @galaxies = galaxies( @map );

    my @distances;

    while ( my $galaxy1 = pop @galaxies ) {
        foreach my $galaxy2 ( @galaxies ) {
            push @distances, abs( $galaxy1->[0] - $galaxy2->[0] ) + abs( $galaxy1->[1] - $galaxy2->[1] );
        }
    }

    return sum0( @distances );
}


sub dumpMap {
    my @map = @_;

    foreach my $row ( @map ) {
        warn join('', @$row ). "\n";
    }
}


sub expandRows {
    my @map = @_;

    my @expandedMap = map { join('', @$_ ) =~ m/#/ ?  $_ : ( $_, $_ ) } @map;

    return @expandedMap;
}


sub expandCols {
    my @map = @_;

    for( my $i = 0; $i < scalar @{$map[0]}; $i++ ) {
        if ( none { $_ eq '#' } map { $_->[ $i ] } @map ) {
            @map = map { [@$_[0..$i], '.', ($i <= $#$_ ? @$_[ ($i + 1) .. $#$_ ] : () ) ] } @map;
            $i++;
        }
    }

    return @map;
}


sub galaxies {
    my @map = @_;
    my @galaxies;

    for( my $row = 0; $row < @map; $row++ ) {
        for( my $col = 0; $col < @{$map[$row]}; $col++ ) {
            if ( $map[$row][$col] eq '#' ) {
                push @galaxies, [ $row, $col ];
            }
        }
    }

    return @galaxies;
}


sub part2 {
    my ( $file ) = @_;

    my @map = loadCharacterMap( $file );
    my @rowExpansions = rowExpansions( $expansion, @map );
    my @colExpansions = colExpansions( $expansion, @map );


    my $pos = positionFinder( \@map, \@rowExpansions, \@colExpansions );

    my @galaxies = map { $pos->( $_ ) } galaxies( @map );

    my @distances;

    while ( my $galaxy1 = pop @galaxies ) {
        foreach my $galaxy2 ( @galaxies ) {
            push @distances, abs( $galaxy1->[0] - $galaxy2->[0] ) + abs( $galaxy1->[1] - $galaxy2->[1] );
        }
    }

    return sum0( @distances );
}


sub rowExpansions {
    my ( $expansion, @map ) = @_;

    my @expansions = map { join('', @$_ ) =~ m/#/ ?  1 : $expansion } @map;

    return @expansions;
}


sub colExpansions {
    my ( $expansion, @map ) = @_;

    my @expansions;

    for( my $i = 0; $i < scalar @{$map[0]}; $i++ ) {
        push @expansions, ( any { $_ eq '#' } map { $_->[ $i ] } @map ) ? 1 : $expansion;
    }

    return @expansions;
}


sub positionFinder {
    my ( $map, $rowExpansions, $colExpansions ) = @_;

    return sub {
        my ( $pos ) = @_;

        return [
            sum0( @$rowExpansions[0..$pos->[0]] ),
            sum0( @$colExpansions[0..$pos->[1]] ),
        ];
    }
}
1;
