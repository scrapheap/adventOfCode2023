package Day_01;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use List::Util qw( sum0 );

use Utils qw( loadList );

sub part1 {
    my ( $file ) = @_;

    my @instructions = loadList( $file );

    my @numbers = map { extractNumber( $_ ) } @instructions;

    return sum0( @numbers );
}


sub extractNumber {
    my ( $string ) = @_;
    
    return ( $_ =~ s/^\D*(\d).*$/$1/r ) . ( $_ =~ s/^.*(\d)\D*$/$1/r );
}


sub part2 {
    my ( $file ) = @_;

    my @instructions = loadList( $file );

    my @numbers = map { extractNumber2( $_ ) } @instructions;

    return sum0( @numbers );
}


sub extractNumber2 {
    my ( $string ) = @_;

    my %digit = (
        0   => 0, 1     => 1, 2     => 2, 3    => 3, 4    => 4,
        5   => 5, 6     => 6, 7     => 7, 8    => 8, 9    => 9,
        one => 1, two   => 2, three => 3, four => 4, five => 5,
        six => 6, seven => 7, eight => 8, nine => 9,
    );

    my $source = join( '|', keys %digit );

    return ( $_ =~ s/^.*?($source).*$/$digit{$1}/r ) . ( $_ =~ s/^.*($source).*?$/$digit{$1}/r );
}

1;
