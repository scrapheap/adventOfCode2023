package Day_02;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use List::Util qw( sum0 all max );

use Utils qw( loadList );


sub part1 {
    my ( $file ) = @_;

    my @games = loadList( $file );

    my $availableCubes = {
        red => 12,
        green => 13,
        blue => 14,
    };

    my @possibleGames = grep { isPossibleGame( $availableCubes, $_ ) } @games;

    my @possibleGameIds = map { $_ =~ m/^Game\s+(\d+)/ } @possibleGames;

    return sum0( @possibleGameIds );
}


sub isPossibleGame {
    my ( $available, $game ) = @_;

    return all { isPossibleSet( $available, $_ ) }  extractSets( $game );
}


sub extractSets {
    my ( $game ) = @_;

    my ( $setLists ) = $game =~ m/^Game\s+\d+\s*:\s*(.*)$/;

    return split /\s*;\s*/, $setLists;
}


sub isPossibleSet {
    my ( $available, $set ) = @_;

    my @samples = split /\s*,\s*/, $set;

    foreach my $sample ( @samples ) {
        my ( $count, $colour ) = $sample =~ m/(\d+)\s+(\w+)/;

        return 0  if  $available->{$colour} < $count;
    }

    return 1;
}


sub part2 {
    my ( $file ) = @_;

    my @games = loadList( $file );

    my @gamePowers = map { gamePower( $_ ) } @games;

    return sum0( @gamePowers );
}


sub gamePower {
    my ( $game ) = @_;

    my @sets = extractSets( $game );

    my %minimalCubes = ( red => [], green => [], blue => [] );

    foreach my $set ( @sets ) {
        my @samples = split /\s*,\s*/, $set;

        foreach my $sample ( @samples ) {
            my ( $count, $colour ) = $sample =~ m/(\d+)\s+(\w+)/;

            push @{ $minimalCubes{ $colour } }, $count;
        }
    }

    return
        max( @{ $minimalCubes{ red } } )
        * max( @{ $minimalCubes{ green } } )
        * max( @{ $minimalCubes{ blue } } );

}

1;
