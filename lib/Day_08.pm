package Day_08;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadList );
use Math::Utils qw( lcm );

sub part1 {
    my ( $file ) = @_;

    my ( $instructions, @nodeDescriptions ) = loadList( $file );

    my $nextDirection = directions( $instructions );

    my %node = map { $_ ? node( $_ ) : () } @nodeDescriptions;

    my $steps = 0;
    my $loc = 'AAA';

    while ($loc ne 'ZZZ') {
        $steps++;
        $loc = $node{ $loc }->{ $nextDirection->() };
    }

    return $steps;
}


sub directions {
    my ( $instructions ) = @_;

    my @directions = split //, $instructions;

    return sub {
        my $direction = shift @directions;
        push @directions, $direction;
        return $direction;
    };
}


sub node {
    my ( $nodeDescription ) = @_;

    my ( $node, $left, $right ) = $nodeDescription =~ m/(\w+)\s*=\s*\((\w+)\s*,\s*(\w+)\)/;

    return $node => { L => $left, R => $right };
}

sub part2 {
    my ( $file ) = @_;

    my ( $instructions, @nodeDescriptions ) = loadList( $file );

    my %node = map { $_ ? node( $_ ) : () } @nodeDescriptions;

    my @locs = grep { m/A$/ } keys %node;

    my @distances = map { distance( $_, $instructions, %node ) } @locs;

    return lcm( @distances );
}


sub distance {
    my ( $loc, $instructions, %node ) = @_;

    my $nextDirection = directions( $instructions );

    my $steps = 0;

    while ( $loc !~ m/Z$/ ) {
        $steps++;
        $loc = $node{ $loc }->{ $nextDirection->() };
    }

    return $steps;
}

1;
