package Day_05;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use List::Util qw( min reduce );

use Utils qw( loadList );


sub part1 {
    my ( $file ) = @_;

    my @almanac = loadList( $file );

    my @groups = ( [] );

    foreach my $line ( @almanac ) {
        if ( $line eq "" ) {
            push @groups, [];
        } else {
            push @{ $groups[$#groups] }, $line;
        }
    }
    
    my ( $seedList, @mappings ) = @groups;

    my @seeds = map { $_ =~ m/(\d+)/g } @$seedList;

    my %mapping = map { mapping( $_ ) } @mappings;

    my @mappingOrder = (
        'seed-to-soil',
        'soil-to-fertilizer',
        'fertilizer-to-water',
        'water-to-light',
        'light-to-temperature',
        'temperature-to-humidity',
        'humidity-to-location',
    );
    
    foreach my $map ( @mappingOrder ) {
        @seeds = map { $mapping{ $map }->( $_ ) } @seeds;
    }

    return min( @seeds );
}


sub mapping {
    my ( $mapping ) = @_;

    my $label = shift @$mapping;

    my ( $name ) = $label =~ m/^(.*?) map:/;

    my @range = map { [ split /\s+/ ] } @$mapping;

    return $name => sub {
        my ( $in ) = @_;

        foreach my $entry ( @range ) {
            if( $in >= $entry->[1] && $in <= ( $entry->[1] + $entry->[2] ) ) {
                return $entry->[0] + $in - $entry->[1];
            }
        }

        return $in;
    };
}

sub part2 {
    my ( $file ) = @_;

    my @almanac = loadList( $file );

    my @groups = ( [] );

    foreach my $line ( @almanac ) {
        if ( $line eq "" ) {
            push @groups, [];
        } else {
            push @{ $groups[$#groups] }, $line;
        }
    }
    
    my ( $seedList, @mappings ) = @groups;

    my @seedRanges;
    foreach my $range ( $seedList->[0] =~m/(\d+\s+\d+)/g ){
        my ( $from, $size ) = split /\s+/, $range;
        push @seedRanges, seedRange( $from, $size );
    }

    my %mapping = map { mappingRange( $_ ) } @mappings;

    my @mapOrder = ( 
        'seed-to-soil',
        'soil-to-fertilizer',
        'fertilizer-to-water',
        'water-to-light',
        'light-to-temperature',
        'temperature-to-humidity',
        'humidity-to-location',
    );
    
    foreach my $map ( @mapOrder ) {
        @seedRanges = applyMap( \@seedRanges, $mapping{ $map } );
        @seedRanges = sort { $a->{from} <=> $b->{from} } @seedRanges;
    }

    return min( map { $_->{from} } @seedRanges );
}

sub mappingRange {
    my ( $mapping ) = @_;

    my $label = shift @$mapping;

    my ( $name ) = $label =~ m/^(.*?) map:/;

    my @range = map { mappingEntry( split /\s+/ ) } @$mapping;
    @range = sort { $a->{sourceFrom} <=> $b->{sourceFrom } } @range;

    return $name => \@range;
}

sub mappingEntry {
    my ( $dest, $source, $size ) = @_;

    return {
        sourceFrom => $source,
        sourceTo   => $source + $size - 1,
        dest       => $dest,
    };
}

sub seedRange {
    my ( $from, $size ) = @_;

    return {
        from => $from,
        to   => $from + $size,
    };
}


sub applyMap {
    my ( $seedRanges, $map ) = @_;

    my @newRanges;

    RANGE:
    while( my $range = shift @$seedRanges ) {
        foreach my $entry ( @$map ) {
            if ( covers( $range, $entry ) ) {
                push @newRanges, mapToNewRange( $range, $entry );
                next RANGE;
            }
            if ( overlapping( $range, $entry ) ) {
                unshift @$seedRanges, splitRange( $range, $entry );
                next RANGE;
            }
        }

        push @newRanges, $range;
    }

    return @newRanges;
}


sub overlapping {
    my ( $range, $entry ) = @_;
    return $range->{from} <= $entry->{sourceTo}  &&  $range->{to} >= $entry->{sourceFrom};
}


sub covers {
    my ( $range, $entry ) = @_;
    return $entry->{sourceFrom} <= $range->{from}  &&  $entry->{sourceTo} >= $range->{to};
}


sub mapToNewRange {
    my ( $range, $entry ) = @_;

    my $offset = $range->{from} - $entry->{sourceFrom};
    my $size   = $range->{to} - $range->{from};

    return {
        from => $entry->{dest} + $offset,
        to   => $entry->{dest} + $offset + $size,
    };
}


sub splitRange {
    my ( $range, $entry ) = @_;

    my $split;
    
    if ( $range->{from} < $entry->{sourceFrom} ) {
        $split = $entry->{sourceFrom} - 1;
    } else {
        $split = $entry->{sourceTo};
    }

    return (
        {
            from => $range->{from},
            to   => $split,
        }, {
            from => $split + 1,
            to   => $range->{to},
        }
    );
}

1;
