package Day_18;

use v5.36;
use feature 'multidimensional';

use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadList );

use List::Util qw( min max sum0 );

sub part1 {
    my ( $file ) = @_;

    my @route = loadList( $file );

    my $map = {};

    my $x = 0;
    my $y = 0;

    my ($minX, $maxX, $minY, $maxY) = ( 0, 0, 0, 0 );

    my $moves= {
        U => sub { return ( $_[0], $_[1] - 1 ) },
        D => sub { return ( $_[0], $_[1] + 1 ) },
        L => sub { return ( $_[0] - 1, $_[1] ) },
        R => sub { return ( $_[0] + 1, $_[1] ) },
    };

    my $b = 0;
    foreach my $step ( @route ) {
        my ( $dir, $dist, $colour ) = $step =~ m/^([UDLR])\s+(\d+)\s+(.*)$/;

        for ( 1..$dist ) {
            ( $x, $y ) = $moves->{ $dir }->( $x, $y );
            $map->{ $x, $y } = $colour;
            $b++;

            $minX = min( $minX, $x );
            $minY = min( $minY, $y );
            $maxX = max( $maxX, $x );
            $maxY = max( $maxY, $y );
        }
    }

    my $changing = 1;
    $map->{ $minX - 1, $minY -1 } = "0";

    while ( $changing ) {
        $changing = 0; 

        for my $y ( ( $minY - 1 ) .. ( $maxY + 1) ) {
            for my $x ( ( $minX - 1 ) .. ( $maxX + 1 ) ) {
                if (
                    !defined $map->{$x, $y}
                    && (
                        ( exists $map->{$x - 1, $y} && $map->{$x - 1, $y} eq "0" )
                        || ( exists $map->{$x + 1, $y} && $map->{$x + 1, $y} eq "0" )
                        || ( exists $map->{$x, $y - 1} && $map->{$x, $y - 1} eq "0" )
                        || ( exists $map->{$x, $y + 1} && $map->{$x, $y + 1} eq "0" )
                    )
                ) {
                    $changing = 1;
                    $map->{$x, $y} = 0;
                }
            }
        }
    }

    my $i = 0; 

    for my $y ( $minY..$maxY ) {
        for my $x ( $minX..$maxX ) {
            if ( ! defined $map->{$x, $y} ) {
                $i++;
            }
        }
    }

    return $i + $b;
}


sub part2 {
    my ( $file ) = @_;

    my @route = map { fix( $_ ) } loadList( $file );

    my $polygon = [];

    my $moves= {
        U => sub { return ( $_[0], $_[1] - $_[2] ) },
        D => sub { return ( $_[0], $_[1] + $_[2] ) },
        L => sub { return ( $_[0] - $_[2], $_[1] ) },
        R => sub { return ( $_[0] + $_[2], $_[1] ) },
    };

    my ( $x, $y ) = ( 0, 0 );
    my $b = 0;

    foreach my $edge ( @route ) {
        my ( $dir, $dist ) = split /\s+/, $edge;

        push @$polygon, [ $x, $y ];

        $b += $dist;

        ( $x, $y ) = $moves->{$dir}->( $x, $y, $dist );
    }

    my $i = shoelace( $polygon );

    return picks( $i, $b );
}

my @dirs = qw( R D L U );

sub fix {
    my ( $line ) = @_;

    my ( $hex, $dir ) = $line =~ m/#([0-9a-f]{5})([0-9a-f])/;

    return join( " ", $dirs[$dir], hex( $hex ) );
}


sub shoelace {
    my ( $polygon ) = @_;

    my @sums = (0, 0);

    my $l = @$polygon;

    for( my $v = 0; $v < $l; $v++ ) {
        $sums[0] += ( $polygon->[$v]->[0] * $polygon->[ ($v+1) % $l ]->[1] );
        $sums[1] += ( $polygon->[$v]->[1] * $polygon->[ ($v+1) % $l ]->[0] );
    }

    return abs( $sums[1] - $sums[0] ) / 2;
}


sub picks {
    my ( $i, $b ) = @_;

    return $i + ( $b / 2 ) + 1;
}

1;
