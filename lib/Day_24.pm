package Day_24;

use v5.36;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadList );
use List::Util qw( min max );
use List::MoreUtils qw(slide minmax);

sub part1 {
    my ( $file, $testRange ) = @_;

    $testRange //= [200000000000000, 400000000000000];
    my $stoneList = loadList( $file );

    my @stones;

    foreach my $line ( @$stoneList ) {
        my ( $x, $y, $z, $vx, $vy, $vz ) = split /\s*[,@]\s*/, $line;

        my $g = $vy / $vx;
        my $stone = {
            line => $line,
            x => $x, y => $y, z => $z,
            vx => $vx, vy => $vy, vz => $vz,
            g => $g,
            c => $y - ($x * $g),
        };

        push @stones, $stone;
    }

    my $crossInRange = 0;
    for( my $s1 = 0; $s1 < @stones; $s1++ ) {
        STONE:
        for( my $s2 = $s1 + 1; $s2 < @stones; $s2++ ) {
            my ( $stone1, $stone2 ) = @stones[ $s1, $s2 ];

            next STONE if $stone1->{g} == $stone2->{g};
            
            my $x = ($stone2->{c} - $stone1->{c}) / ($stone1->{g} - $stone2->{g});

            if ( $testRange->[0] <= $x <= $testRange->[1] ) {
                my $y = ( $x * $stone1->{g} ) + $stone1->{c};

                if ( $testRange->[0] <= $y <= $testRange->[1] ) {
                    if( ! ( inPast( $stone1, $x, $y ) || inPast( $stone2, $x, $y ) ) ) {
                        $crossInRange++;
                    }
                }
            }
        }
    }

    return $crossInRange;
}


sub inPast {
    my ( $stone, $x, $y ) = @_;

    if (
        ( $stone->{vx} < 0 && $stone->{x} < $x ) ||
        ( $stone->{vx} > 0 && $stone->{x} > $x )
    ) {
        if(
            ( $stone->{vy} < 0 && $stone->{y} < $y ) ||
            ( $stone->{vy} > 0 && $stone->{y} > $y )
        ) {
            return 1;
        }
    }

    return 0;
}

sub part2 {
    my ( $file ) = @_;

    my $stoneList = loadList( $file );

    my @stones;

    foreach my $line ( @$stoneList ) {
        my ( $x, $y, $z, $vx, $vy, $vz ) = split /\s*[,@]\s*/, $line;

        my $stone = {
            line => $line,
            x => $x, y => $y, z => $z,
            vx => $vx, vy => $vy, vz => $vz,
        };

        push @stones, $stone;
    }


    # Heavily based on ProfONeill's code
    #   - https://www.reddit.com/r/adventofcode/comments/18pnycy/comment/kf2hgde/

    # Find the min and max velocity for each axis
    my @minmaxes = map { my $col = $_; [minmax map { $_->{$col} } @stones] } (qw(vx vy vz));
    my $maxVelocity = max map { map { abs($_) } @$_ } @minmaxes;

    my @colnames = qw(x y z vx vy vz);
    my %vToP=( vx => 'x', vy => 'y', vz => 'z' );
    my @vcands;
    foreach my $col (qw( vx vy vz )) {
        my %positionForVelcity;
        push @{$positionForVelcity{$_->{$col}}}, $_->{ $vToP{$col} } foreach @stones;
        my %candidates;
        my $count = 0;
        foreach my $vhail (keys %positionForVelcity) {
            my $posns = $positionForVelcity{$vhail};
            next if @$posns < 2;
            ++$count;
            foreach my $vrock (-$maxVelocity..$maxVelocity) {
                slide {
                    my $dist = $b - $a;
                    my $denom = $vhail - $vrock;
                    ++$candidates{$vrock}{$vhail} if $denom != 0 && $dist % $denom == 0;
                } sort { $a <=> $b } @$posns;
            }
        }
        my @candidates;
        foreach my $vrock (keys %candidates) {
            my $vhails = $candidates{$vrock};
            next if keys %$vhails < $count;
            push @candidates, $vrock;
        }
        push @vcands, \@candidates;
    }

    # Find two distinct hailstones with different velocities
    my $stone1 = shift @stones or die; # Hailstone A
    my $stone2;
    while (1) {
        $stone2 = shift @stones or die; # Hailstone B
        last unless $stone1->{vx} == $stone2->{vx} || $stone1->{vy} == $stone2->{vy} || $stone1->{vz} == $stone2->{vz};
    }


    my ($sx,$sy,$sz,$t);
    OUTER: foreach my $rvx (@{$vcands[0]}) {
        foreach my $rvy (@{$vcands[1]}) {
            foreach my $rvz (@{$vcands[2]}) {
                ($sx,$sy,$sz) = solve($rvx,$rvy,$rvz, $stone1, $stone2);
                last OUTER if (defined $sx);
            }
        }
    }

    return $sx+$sy+$sz;
}

sub intersect {
    my ($x1,$y1,$vx1,$vy1, $x2,$y2,$vx2,$vy2) = @_;
    my $denom = $vy2*$vx1 - $vx2*$vy1;
    if ($denom == 0) {
        return undef;
    }
    my $ua = ($vx2*($y1-$y2) - $vy2*($x1-$x2)) / $denom;
    my $ub = ($vx1*($y1-$y2) - $vy1*($x1-$x2)) / $denom;
    return $x1 + $ua*$vx1, $y1 + $ua*$vy1, $ua;
}

sub solve {
    my ($rvx, $rvy, $rvz, $stone1, $stone2 ) = @_;
    # Relative velocity of A to the rock
    my ($relX1,$relY1,$relZ1) = ($stone1->{vx}-$rvx, $stone1->{vy}-$rvy, $stone1->{vz}-$rvz);
    my $tdelta = 0;
    my ($relX2,$relY2,$relZ2) = ($stone2->{vx}-$rvx, $stone2->{vy}-$rvy, $stone2->{vz}-$rvz);
    my ($sx,$sy,$t1,$ignored1) = intersect($stone1->{x},$stone1->{y},$relX1,$relY1, $stone2->{x},$stone2->{y},$relX2,$relY2);
    my ($sxAlt,$sz,$t2,$ignored2) = intersect($stone1->{x},$stone1->{z},$relX1,$relZ1, $stone2->{x},$stone2->{z},$relX2,$relZ2);
    return undef unless defined $sx && defined $sxAlt && $sx == $sxAlt;
    die unless $t1 == $t2;
    return $sx, $sy, $sz;
}

1;
