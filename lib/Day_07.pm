package Day_07;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadList );
use Readonly;
use List::Util qw( reduce );

Readonly my %cardValue => (
    _ => 1,
    2 => 2,
    3 => 3,
    4 => 4,
    5 => 5,
    6 => 6,
    7 => 7,
    8 => 8,
    9 => 9,
    T => 10,
    J => 11,
    Q => 12,
    K => 13,
    A => 14,
);

sub part1 {
    my ( $file ) = @_;

    my @bidList = parseHands( loadList( $file ) );

    @bidList = sort { compareHands( $a, $b ) } @bidList;

    my $rank = 0;
    return reduce { $a + $b->{bid} * (++$rank) } 0, @bidList;
}


sub part2 {
    my ( $file ) = @_;

    my @bidList = parseHandsWithJokers( loadList( $file ) );

    @bidList = sort { compareHands( $a, $b ) } @bidList;

    my $rank = 0;
    return reduce { $a + $b->{bid} * (++$rank) } 0, @bidList;
}


sub parseHands {
    return map { my ( $hand, $bid ) = split /\s+/, $_;  { hand => [ split //, $hand], bid => $bid } } @_;
}

sub parseHandsWithJokers {
    return map { my ( $hand, $bid ) = split /\s+/, $_;  { hand => [ split //, $hand =~ s/J/_/gr ], bid => $bid } } @_;
}

sub compareHands {
    my ( $left, $right ) = @_;

    return handType( $left->{hand} ) <=> handType( $right->{hand} ) || compareCards( $left->{hand}, $right->{hand} );
}


sub handType {
    my ( $hand ) = @_;

    my %cardGroups;

    foreach my $card ( @$hand ) {
        $cardGroups{$card}++;
    }

    my $jokers = delete $cardGroups{'_'} // 0;

    my @counts = sort { $b <=> $a } values %cardGroups;

    if ( @counts <= 1 ) {
        return 7;
    }

    if ( @counts == 2 && $counts[0] + $jokers == 4 ) {
        return 6;
    }

    if ( @counts == 2 && $counts[0] + $jokers == 3 ) {
        return 5;
    }

    if ( @counts == 3 && $counts[0] + $jokers == 3 ) {
        return 4;
    }

    if ( @counts == 3 && $counts[0] + $jokers == 2 ) {
        return 3;
    }

    if ( @counts == 4 ) {
        return 2;
    }

    return 1;
}


sub compareCards {
    my ( $left, $right ) = @_;

    for my $i ( 0..4 ) {
        my $winner = $cardValue{ $left->[ $i ] } - $cardValue{ $right->[$i] };

        next if $winner == 0;
        return $winner;
    }

    return 0
}
1;
