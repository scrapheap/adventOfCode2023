package Day_21;

use v5.36;
use feature 'multidimensional';

use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadCharacterMap  maxXY  dumpMap );
use List::Util qw( any );

sub part1 {
    my ( $file, $steps ) = @_;

    my $map = [ loadCharacterMap( $file ) ];

    $steps //= 64;

    for( 1..$steps ) {
        $map = nextStep( $map );
    }

    return scalar ( grep { $_ eq "O" } map { @$_ } @$map );
}


sub nextStep {
    my ( $map ) = @_;

    my ( $maxX, $maxY ) = map { $_ - 1 } maxXY( @$map );
    my $newMap = [];

    for my $y ( 0..$maxY ) {
        for my $x ( 0..$maxX ) {
            $newMap->[$y]->[$x] = $map->[$y]->[$x];

            next if $map->[$y]->[$x] eq "#";

            if (
                ( $x > 0 && ( $map->[$y][$x - 1] eq "O" || $map->[$y][$x - 1] eq "S" ) )
                || ( $y > 0 && ( $map->[$y - 1][$x] eq "O" || $map->[$y - 1][$x] eq "S" ) )
                || ( $x < $maxX && ( $map->[$y][$x + 1] eq "O" || $map->[$y][$x + 1] eq "S" ) )
                || ( $y < $maxY && ( $map->[$y + 1][$x] eq "O" || $map->[$y  + 1][$x] eq "S" ) )
            ) {
                $newMap->[$y]->[$x] = "O";
            } else {
                $newMap->[$y]->[$x] = ".";
            }
        }
    }

    return $newMap;
}

sub neighbours {
    my ( $x, $y, $maxX, $maxY ) = @_;

    my @neighbours;

    if ( $x > 0 ) {
        push @neighbours, [ $x - 1, $y ];
    }

    if ( $y > 0 ) {
        push @neighbours, [ $x, $y - 1 ];
    }

    if ( $x < $maxX ) {
        push @neighbours, [ $x + 1, $y ];
    }

    if ( $y < $maxY ) {
        push @neighbours, [ $x, $y + 1 ];
    }

    return @neighbours;
}

sub part2 {
    my ( $file, $steps ) = @_;

    $steps //= 26501365;
    my $map = [ loadCharacterMap( $file ) ];

    my ( $maxX, $maxY ) = maxXY( $map );

    my $newMap = [];

    for my $row( 0..4 ) {
        for my $col ( 0..4 ) {
            for( my $y = 0; $y < $maxY; $y++ ) {
                for( my $x = 0; $x < $maxX; $x++ ) {
                    if ( ( $row != 2 || $col != 2 ) && $map->[$y]->[$x] eq "S" ) {
                        $newMap->[( $row * $maxY ) + $y]->[( $col * $maxX) + $x] = ".";
                    } else {
                        $newMap->[( $row * $maxY ) + $y]->[( $col * $maxX) + $x] = $map->[$y]->[$x];
                    }
                }
            }
        }
    }

    $map = $newMap;

    my $initialPoint = ( $maxX - 1 )  / 2;
    my @requiredCounts = (  $initialPoint, $initialPoint + $maxX, $initialPoint + ( $maxX * 2 ) );
    my $iSteps = $requiredCounts[-1];
    
    my @counts;
    my $nextCount = 0;
    for my $step ( 1..$iSteps ) {
        print "$step / $iSteps\r";
        $map = nextStep( $map );
        if ( $step == $steps ) {
            return scalar ( grep { $_ eq "O" } map { @$_ } @$map );
        }
        if ( $step == $requiredCounts[ $nextCount ] ) {
            push @counts, scalar ( grep { $_ eq "O" } map { @$_ } @$map );

            $nextCount++;
        }
    }

    my $b0 = $counts[0];
    my $b1 = $counts[1] - $counts[0];
    my $b2 = $counts[2] - $counts[1];

    my $grids = int( $steps / $maxX );

    return $b0 + $b1* $grids + int( $grids * ( $grids - 1 ) / 2 ) * ($b2 - $b1);
}

1;
