package Day_20;

use v5.36;
use feature 'multidimensional';

use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadList );
use Math::Utils qw( lcm );

use Day_20::Constants qw( LOW HIGH );
use Day_20::Node;


sub part1 {
    my ( $file ) = @_;

    my @schema = loadList( $file );


    my %node;

    foreach my $connections ( @schema ) {
        my ( $type, $name, $dests ) = $connections =~ m/^([%&]?)(\w+)\s*->\s*(.*)/;

        $node{ $name } //= Day_20::Node->new( $name );
        $node{ $name }->type( $type );
        foreach my $dest( split /\s*,\s*/, $dests ) {
            $node{ $name }->addOutput( $dest );

            $node{ $dest } //= Day_20::Node->new( $dest );
            $node{ $dest }->addInput( $name );
        }
    }

    $node{broadcaster}->addInput( 'button' );
    my @pulseCounts = (0,0);

    for( 1..1000 ) {
        my @pulses = $node{broadcaster}->setInput( 'button', LOW );
        $pulseCounts[0]++;

        while ( my $pulse = shift @pulses ) {
            $pulseCounts[$pulse->[2]]++;
            push @pulses, $node{ $pulse->[1] }->setInput( $pulse->[0], $pulse->[2] );
        }
    }

    return $pulseCounts[0] * $pulseCounts[1];
}


sub part2 {
    my ( $file ) = @_;

    my @schema = loadList( $file );

    my %node;

    foreach my $connections ( @schema ) {
        my ( $type, $name, $dests ) = $connections =~ m/^([%&]?)(\w+)\s*->\s*(.*)/;

        $node{ $name } //= Day_20::Node->new( $name );
        $node{ $name }->type( $type );
        foreach my $dest( split /\s*,\s*/, $dests ) {
            $node{ $name }->addOutput( $dest );

            $node{ $dest } //= Day_20::Node->new( $dest );
            $node{ $dest }->addInput( $name );
        }
    }

    my ( $keyComponent ) = keys %{ $node{rx}->{input} };
    my %cycle = map { $_ => undef } keys %{ $node{$keyComponent}->{input} };

    $node{broadcaster}->addInput( 'button' );
    my $presses = 0;

    PRESS:
    while( 1 ) {
        my @pulses = $node{broadcaster}->setInput( 'button', LOW );
        $presses++;

        while ( my $pulse = shift @pulses ) {
            if ( $pulse->[1] eq $keyComponent && $pulse->[2] == HIGH ) {
                foreach my $component ( keys %cycle ) {
                    $cycle{ $pulse->[0] } //= $presses;
                }
            }
            push @pulses, $node{ $pulse->[1] }->setInput( $pulse->[0], $pulse->[2] );
        }

        last PRESS unless grep { ! defined $_ } values %cycle;
    }

    return lcm( values %cycle );
}

1;
