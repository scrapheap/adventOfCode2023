package Day_19;

use v5.36;
use feature 'multidimensional';

use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadList );
use List::Util qw( sum0 min max );

sub part1 {
    my ( $file ) = @_;

    my @parts = loadList( $file );

    my @workflows;

    while (my $line = shift @parts) {
        last if $line eq "";
        push @workflows, $line;
    }

    my %flow;

    my %rule = (
        '>' => sub {
            my ( $property, $value, $dest ) = @_;

            return sub {
                return $_[0]->{$property} > $value ? $dest : undef;
            }
        },
        '<' => sub {
            my ( $property, $value, $dest ) = @_;

            return sub {
                return $_[0]->{$property} < $value ? $dest : undef;
            }
        },
        'default' => sub {
            my ( $dest ) = @_;

            return sub {
                return $dest;
            }
        }

    );

    foreach my $workflow ( @workflows ) {
        my ( $name, $rules, $default ) = $workflow =~ m/(\w+)\{(.*),(\w+)}/;

        my $chain = [];
        foreach my $instr ( split /,/, $rules ) {
            my ( $property, $op, $value, $dest ) = $instr =~ m/(\w+)([<>])(\d+):(\w+)/;

            push @$chain, $rule{ $op }->( $property, $value, $dest );
        }

        push @$chain, $rule{ default }->( $default );
        $flow{ $name } = $chain;
    }

    my $accepted = 0;

    foreach my $partDef ( @parts ) {
        my $part = { map { $_=~m/(\w)=(\d+)/ } split /,/, $partDef };

        my $dest = "in";
        while( $dest ne "A" && $dest ne "R" ) {
            my $chain = $flow{ $dest };
            RULE:
            foreach my $rule ( @$chain ) {
                $dest = $rule->( $part );
                last if $dest;
            }
        }

        if ( $dest eq "A" ) {
            $accepted += sum0( values %$part );
        }
    }

    return $accepted;
}


sub part2 {
    my ( $file ) = @_;

    my @parts = loadList( $file );

    my %workflow;

    while (my $line = shift @parts) {
        last if $line eq "";
        my ( $name, $rules ) = $line =~ m/^(\w+)\{(.*)/;
        $workflow{ $name } = $rules;
    }

    my $groups={
        in => [ {
            x => [ 1, 4000 ],
            m => [ 1, 4000 ],
            a => [ 1, 4000 ],
            s => [ 1, 4000 ],
        } ],
    };

    while ( my $flow = nextFlow( $groups ) ) {
        my ( $rules, $default ) = $workflow{ $flow } =~ m/(.*),(\w+)}/;
        my @instrs = map { [ $_ =~ m/(\w+)([<>])(\d+):(\w+)/ ] } split /,/, $rules;

        while( my $group = shift @{ $groups->{ $flow } } ) {
            foreach my $instr ( @instrs ) {
                my ( $property, $op, $value, $dest ) = @$instr;
                
                my $subGroup = cloneGroup( $group );

                if ( $op eq "<" ) {
                    $subGroup->{$property}->[1] = min($subGroup->{$property}->[1], $value - 1);
                    $group->{$property}->[0] = max($group->{$property}->[0], $value);
                } else {
                    $subGroup->{$property}->[0] = max($subGroup->{$property}->[0], $value + 1);
                    $group->{$property}->[1] = min($group->{$property}->[1], $value);
                }

                if ( validRanges( $subGroup ) ) {
                    push @{ $groups->{$dest} }, $subGroup;
                }
            }

            if ( validRanges( $group ) ) {
                push @{ $groups->{ $default } }, $group;
            }
        }

    }


    return sum0( map {
        (
              ( 1 + $_->{x}->[1] - $_->{x}->[0] )
            * ( 1 + $_->{m}->[1] - $_->{m}->[0] )
            * ( 1 + $_->{a}->[1] - $_->{a}->[0] )
            * ( 1 + $_->{s}->[1] - $_->{s}->[0] )
        )
        } @{$groups->{A}}
    );
}

sub validRanges {
    my ( $group ) = @_;

    return $group->{x}->[0] <= $group->{x}->[1]
        && $group->{m}->[0] <= $group->{m}->[1]
        && $group->{a}->[0] <= $group->{a}->[1]
        && $group->{s}->[0] <= $group->{s}->[1];
}

sub nextFlow {
    my ( $groups ) = @_;

    foreach my $workflow (grep { $_ ne "A" && $_ ne "R" } keys %$groups ) {
        if ( scalar @{ $groups->{ $workflow } } ) {
            return $workflow;
        }
    }

    return undef;
}

sub cloneGroup {
    my ( $group ) = @_;

    my $newGroup = {};

    foreach my $property ( keys %$group ) {
        $newGroup->{ $property } = [ @{ $group->{ $property } } ];
    }

    return $newGroup;
}

1;
