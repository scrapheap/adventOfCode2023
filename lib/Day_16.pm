package Day_16;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadCharacterMaps );
use List::Util qw( sum0 max );

use constant NORTH => 1;
use constant EAST  => 2;
use constant SOUTH => 4;
use constant WEST  => 8;

use Day_16::EnergizedMap;

sub part1 {
    my ( $file ) = @_;

    my ( $map ) = loadCharacterMaps( $file );

    my $eMap = Day_16::EnergizedMap->new( $map );

    my @beams = ( newBeam( 0, 0, EAST ) );

    my $seen = {};

    while( @beams ) {
        @beams = map { processBeam( $_, $map, $eMap, $seen ) } @beams;
    }

    return $eMap->energizedCells();
}


sub part2 {
    my ( $file ) = @_;

    my ( $map ) = loadCharacterMaps( $file );

    my $attempt = tryFrom( $map );

    my $maxEnergized = 0;

    my $maxX = scalar( @{ $map->[0] } ) - 1;
    my $maxY = scalar( @$map ) - 1;

    foreach my $x ( 0 .. $maxX ) {
        $maxEnergized = max(
            $maxEnergized,
            $attempt->( newBeam( $x, 0, SOUTH ) ),
            $attempt->( newBeam( $x, $maxY, NORTH ) ),
        );
    }

    foreach my $y ( 0 .. $maxY ) {
        $maxEnergized = max(
            $maxEnergized,
            $attempt->( newBeam( 0, $y, EAST ) ),
            $attempt->( newBeam( $maxX, $y, WEST ) ),
        );
    }

    return $maxEnergized;
}

sub tryFrom {
    my ( $map ) = @_;

    return sub {
        my @beams = @_;

        my $eMap = Day_16::EnergizedMap->new( $map );

        my $seen = {};

        while( @beams ) {
            @beams = map { processBeam( $_, $map, $eMap, $seen ) } @beams;
        }
        # dumpMap( @$map );

        return $eMap->energizedCells();
    }
}

sub newBeam{
    my ( $x, $y, $direction ) = @_;

    return {
        x => $x,
        y => $y,
        direction => $direction
    };
}

my $mirrors = {
    '/' => {
        NORTH, EAST,
        EAST, NORTH,
        SOUTH, WEST,
        WEST, SOUTH,
    },

    '\\' => {
        NORTH, WEST,
        WEST, NORTH,
        SOUTH, EAST,
        EAST, SOUTH
    },
};

sub processBeam {
    my ( $beam, $map, $eMap, $seen ) = @_;

    if ( $beam->{x} < 0
      || $beam->{y} < 0
      || $beam->{x} >= scalar( @{$map->[0]} )
      || $beam->{y} >= scalar( @$map )
      || $seen->{$beam->{x}}->{$beam->{y}}->{$beam->{direction}}
    ) {
        return ();
    }

    $seen->{$beam->{x}}->{$beam->{y}}->{$beam->{direction}} = 1;
    $eMap->energize( $beam->{x}, $beam->{y} );

    # mirrors

    my $cell = $map->[$beam->{y}]->[$beam->{x}];

    if ( my $reflect = $mirrors->{ $cell } ) {
        $beam->{direction} = $reflect->{ $beam->{direction} };
    }

    # splitters

    if ( $cell eq '-' && $beam->{direction} & ( NORTH | SOUTH ) ) {
        return (
            newBeam( $beam->{x} - 1, $beam->{y}, WEST ),
            newBeam( $beam->{x} + 1, $beam->{y}, EAST ),
        );
    }

    if ( $cell eq '|' && $beam->{direction} & ( EAST | WEST ) ) {
        return (
            newBeam( $beam->{x}, $beam->{y} - 1, NORTH ),
            newBeam( $beam->{x}, $beam->{y} + 1, SOUTH ),
        );
    }

    # empty
    return moveBeam( $beam );
}


sub moveBeam {
    my ( $beam ) = @_;

    if ( $beam->{direction} == NORTH ) {
        return newBeam( $beam->{x}, $beam->{y} - 1, $beam->{direction} );
    }

    if ( $beam->{direction} == EAST ) {
        return newBeam( $beam->{x} + 1, $beam->{y}, $beam->{direction} );
    }

    if ( $beam->{direction} == SOUTH ) {
        return newBeam( $beam->{x}, $beam->{y} + 1, $beam->{direction} );
    }

    return newBeam( $beam->{x} - 1, $beam->{y}, $beam->{direction} );
}

1;
