package Day_13;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadCharacterMaps );
use List::Util qw( sum0 min );

use constant HORIZONTAL => 'h';
use constant VERTICAL   => 'v';


sub part1 {
    my ( $file ) = @_;

    my @maps = loadCharacterMaps( $file );

    my %counts = (
        HORIZONTAL, 0,
        VERTICAL, 0,
    );
        
    my $vertical = 0;

    foreach my $map ( @maps ) {
        my ( $direction, $row ) = reflection( $map, 0 );

        if ( defined $direction ) {
            $counts{ $direction } += $row;
        }
    }

    return $counts{VERTICAL()} + ( $counts{HORIZONTAL()} * 100 );
}


sub reflection {
    my ( $map, $threshold ) = @_;

    my @mapStrings = map { join( '', @$_ ) } @$map;

    my $row = findReflection( $threshold, @mapStrings );

    if ( $row ) {
        return ( HORIZONTAL, $row );
    }

    @mapStrings = ();
    for( my $i = 0; $i < @{$map->[0]}; $i++) {
        push @mapStrings, join( '', map { $_->[ $i ] } @$map );
    }

    $row = findReflection( $threshold, @mapStrings );
    
    if ( $row ) {
        return ( VERTICAL, $row );
    }

    return;
}


sub findReflection {
    my ( $threshold, @rows ) = @_;

    for( my $row = 1; $row < @rows; $row++ ) {
        if ( isReflection( $threshold, $row, @rows ) ) {
            return $row;
        }
    }

    return undef;
}


sub isReflection {
    my ( $threshold, $row, @rows ) = @_;

    my $distance = min( $row, abs( $row - scalar @rows ) );

    for( my $i = 0; $i < $distance; $i++ ) {
        my $differences = differences( $rows[ ( $row - 1 ) - $i ], $rows[ $row + $i ] );
        $threshold -= $differences;
    }

    return $threshold == 0;
}


sub differences {
    my ( $row1, $row2 ) = @_;

    my $differences = 0;

    for( my $i =0; $i < length $row1; $i++ ) {
        if ( substr( $row1, $i, 1 ) ne substr( $row2, $i, 1 ) ) {
            $differences++;
        }
    }

    return $differences;
}


sub part2 {
    my ( $file ) = @_;

    my @maps = loadCharacterMaps( $file );

    my %counts = (
        HORIZONTAL, 0,
        VERTICAL, 0,
    );
        
    my $vertical = 0;

    foreach my $map ( @maps ) {
        my ( $direction, $row ) = reflection( $map, 1 );

        if ( defined $direction ) {
            $counts{ $direction } += $row;
        }
    }

    return $counts{VERTICAL()} + ( $counts{HORIZONTAL()} * 100 );
}

1;
