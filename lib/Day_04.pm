package Day_04;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use List::Util qw( sum0 uniq );

use Utils qw( loadList );


sub part1 {
    my ( $file ) = @_;

    my @cards = loadList( $file );

    my @winningNumbers = map { countWinningNumbers( $_ ) } @cards;

    my @scores = map { scoreMatches( $_ ) } @winningNumbers;

    return sum0( @scores );
}


sub countWinningNumbers {
    my ( $card ) = @_;
    
    my ( @numbers ) = map { split /\s+/, $_ } $card =~ m/^Card\s+\d+\s*:\s*(.*?)\s*\|\s*(.*)\s*$/;

    my @uniqNumbers = uniq @numbers;

    return (scalar @numbers) - (scalar @uniqNumbers);
}


sub scoreMatches {
    my ( $matchCount ) = @_;

    return  $matchCount
        ?  1 << ($matchCount - 1) 
        :  0;
}


sub part2 {
    my ( $file ) = @_;

    my @cards = loadList( $file );
    my @cardCounts = map { 1 } @cards;

    my $limit = @cards;

    for( my $card = 0; $card < $limit; $card++ ) {
        my $cardsWon = countWinningNumbers( $cards[ $card ] );

        for( my $offset = 1;  $offset <= $cardsWon  && ( $card + $offset < $limit ); $offset++ ) {
            $cardCounts[ $card + $offset ] += $cardCounts[ $card ];
        }
    }

    return sum0( @cardCounts );
}


1;
