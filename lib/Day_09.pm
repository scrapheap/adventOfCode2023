package Day_09;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadList );
use List::Util qw( sum0  any  reduce );

sub part1 {
    my ( $file ) = @_;

    my @sensors = map { [ m/(\-?\d+)/g ] } loadList( $file );

    my @extrapolatedReadings = map { extrapolate( @$_ ) } @sensors;

    return sum0( @extrapolatedReadings );
}

sub extrapolate {
    my @readings = @_;

    my @levels = ( \@readings );
    my @lastRow = @readings;

    while ( any { $_ != 0 } @lastRow ) {
        my @nextRow;
        
        reduce { push @nextRow, $b - $a;  $b  } @lastRow;
        push @levels, \@nextRow;
        @lastRow = @nextRow;
    }

    my @lastValues = map { pop @$_ } reverse @levels;

    return sum0 (@lastValues);
}
    


sub part2 {
    my ( $file ) = @_;

    my @sensors = map { [ m/(\-?\d+)/g ] } loadList( $file );

    my @extrapolatedReadings = map { extrapolateBackwards( @$_ ) } @sensors;

    return sum0( @extrapolatedReadings );
}

sub extrapolateBackwards {
    my @readings = @_;

    my @levels = ( \@readings );
    my @lastRow = @readings;

    while ( any { $_ != 0 } @lastRow ) {
        my @nextRow;
        
        reduce { push @nextRow, $b - $a;  $b  } @lastRow;
        push @levels, \@nextRow;
        @lastRow = @nextRow;
    }

    my @firstValues = map { shift @$_ } reverse @levels;

    my $extrapolated = reduce { $b - $a } @firstValues;

    return $extrapolated;
}

1;
