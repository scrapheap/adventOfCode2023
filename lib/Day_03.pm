package Day_03;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadCharacterMap );
use List::Util qw( any  max  min sum0 );

sub part1 {
    my ( $file ) = @_;

    my @schema = loadCharacterMap( $file );

    my $rows = scalar @schema;
    my $cols = scalar @{ $schema[0] };

    my $neighbours = neighbourChecker( $rows, $cols, @schema );

    my @numbers;

    for( my $row = 0; $row < $rows; $row++ ) {   
        for( my $col = 0; $col < $cols; $col++ ) {
            if ( $schema[$row][$col] =~ m/\d/ ) {
                my $start = $col;
                my $end = findEndOfNumber( $schema[$row], $col );

                my $number = join('', @{ $schema[$row]}[$start..$end] );

                if ( any { $_ !~ m/[0123456789.]/ } $neighbours->( $row, $start, $end ) ) {
                    push @numbers, $number;
                }

                $col = $end;
            }
        }
    }

    return sum0( @numbers );
}


sub neighbourChecker {
    my ( $rows, $cols, @schema ) = @_;

    return sub {
        my ( $row, $start, $end ) = @_;

        my @neighbours;

        for( my $r = max( $row - 1, 0); $r <= min( $row + 1, $rows - 1 ) ; $r++ ) {
            for( my $c = max( $start - 1, 0 ); $c <= min( $end + 1, $cols - 1 ) ; $c++) {
                push @neighbours, $schema[$r][$c] unless $r == $row && $c >= $start && $c <= $end;
            }
        }

        return @neighbours;
    };
}

sub findEndOfNumber {
    my ( $row, $col ) = @_;

    while ( defined $row->[$col] && $row->[$col] =~ m/\d/ ) {
        $col++;
    }

    return $col - 1;
}

sub part2 {
    my ( $file ) = @_;

    my @schema = loadCharacterMap( $file );

    my $rows = scalar @schema;
    my $cols = scalar @{ $schema[0] };

    my $neighbours = neighbouringGears( $rows, $cols, @schema );

    my @gears;

    for( my $row = 0; $row < $rows; $row++ ) {   
        for( my $col = 0; $col < $cols; $col++ ) {
            if ( $schema[$row][$col] =~ m/\d/ ) {
                my $start = $col;
                my $end = findEndOfNumber( $schema[$row], $col );

                my $number = join('', @{ $schema[$row]}[$start..$end] );

                foreach my $gear ( $neighbours->( $row, $start, $end ) ) {
                    push @{ $gears[$gear->[0]][$gear->[1]] }, $number;
                }

                $col = $end;
            }
        }
    }

    my @gearRatios;

    foreach my $row ( @gears ) {
        push @gearRatios, map { gearRatio( $_ ) } grep { defined } @$row;
    }

    return sum0( @gearRatios );
}


sub gearRatio {
    my ( $gear ) = @_;

    if ( scalar @$gear == 2 ) {
        return $gear->[0] * $gear->[1]
    }
    
    return ();
}

sub neighbouringGears {
    my ( $rows, $cols, @schema ) = @_;

    return sub {
        my ( $row, $start, $end ) = @_;

        my @gears;

        for( my $r = max( $row - 1, 0); $r <= min( $row + 1, $rows - 1 ) ; $r++ ) {
            for( my $c = max( $start - 1, 0 ); $c <= min( $end + 1, $cols - 1 ) ; $c++) {
                if( $schema[$r][$c] eq "*" ) {
                    push @gears, [$r, $c];
                }
            }
        }
        return @gears;
    };
}
1;
