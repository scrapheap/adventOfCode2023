package Day_15;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadList );
use List::Util qw( sum0 min );


sub part1 {
    my ( $file ) = @_;

    my ( $input ) = loadList( $file );

    my @sequence = split /,/, $input;

    return sum0( map { hash( $_ ) } @sequence );
}


sub hash {
    my ( $string ) = @_;

    my $hash = 0;

    foreach my $character ( split //, $string ) {
        $hash += ord( $character );
        $hash *= 17;
        $hash %= 256;
    }

    return $hash;
}


sub part2 {
    my ( $file ) = @_;

    my ( $input ) = loadList( $file );

    my @sequence = split /,/, $input;

    my @boxes = map { [] } ( 0..255 );

    foreach my $instruction ( @sequence ) {
        my ( $label, $op, $focalLength ) = $instruction =~ m/^(\w+?)([-=])(\d*)$/;

        my $box = $boxes[ hash( $label ) ];

        if ( $op eq "-" ) {
            drop( $label, $box );
        } else {
            upsert( $label, $focalLength, $box );
        }
    }

    foreach my $box ( @boxes ) {
        @$box = grep { defined } @$box;
    }

    my @values = map { scoreBox( $boxes[$_], $_ + 1 ) } ( 0..255 );

    return sum0( @values );
}


sub drop {
    my ( $label, $box ) = @_;

    foreach my $entry ( @$box ) {
        if ( defined $entry && $entry->[0] eq $label ) {
            $entry = undef;
            return;
        }
    }

    return;
}


sub upsert {
    my ( $label, $focalLength, $box ) = @_;

    foreach my $entry ( @$box ) {
        if ( defined $entry && $entry->[0] eq $label ) {
            $entry->[1] = $focalLength;
            return;
        }
    }

    push @$box, [ $label, $focalLength ];
    return
}


sub scoreBox {
    my ( $box, $boxNo ) = @_;

    my $total = 0;

    for( my $i = 0; $i < @$box; $i++ ) {
        $total += $boxNo * ( $i + 1 ) * $box->[$i][1];
    }

    return $total;
}

1;
