package Day_10;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadCharacterMap );
use Readonly;

use constant NORTH    => 1<<0;
use constant EAST     => 1<<1;
use constant SOUTH    => 1<<2;
use constant WEST     => 1<<3;
use constant VERTICAL_NORTH => 1<<4;
use constant VERTICAL_SOUTH => 1<<5;
use constant HORIZONTAL_EAST => 1<<6;
use constant HORIZONTAL_WEST => 1<<7;

use constant VERTICAL => VERTICAL_NORTH | VERTICAL_SOUTH;
use constant HORIZONTAL => HORIZONTAL_EAST | HORIZONTAL_WEST;

Readonly my %PIPE => (
    '|' => NORTH | SOUTH,
    '-' => EAST | WEST,
    'F' => SOUTH | EAST,
    '7' => SOUTH | WEST,
    'J' => NORTH | WEST,
    'L' => NORTH | EAST,
);

sub part1 {
    my ( $file ) = @_;

    my @map = loadCharacterMap( $file );

    my ( $startRow, $startCol );

    for( my $row=0; $row < scalar @map; $row++ ) {
        for( my $col=0; $col < scalar @{$map[$row]}; $col++ ) {
            if ( $map[ $row ][ $col ] eq 'S' ) {
                $startRow = $row;
                $startCol = $col;
            } else {
                $map[ $row ][ $col ] = $PIPE{ $map[ $row ][ $col ] } // 0;
            }
        }
    }

    $map[$startRow][$startCol] = guessMissingPipe( \@map, $startRow, $startCol ); 

    my $steps = walkLoop( \@map, $startRow, $startCol, 0 );

    return int( $steps / 2 );
}


sub guessMissingPipe {
    my ( $map, $row, $col ) = @_;

    my $guess = 0;
    # check north
    if( $row > 0  &&  $map->[ $row - 1 ][ $col ] & SOUTH ) {
        $guess |= NORTH;
    }

    # check west
    if ( $col > 0  &&  $map->[ $row ][ $col - 1 ] & EAST ) {
        $guess |= WEST;
    }

    # check south
    if ( $row < $#$map  &&  $map->[ $row + 1 ][ $col ] & SOUTH ) {
        $guess |= SOUTH
    }

    # check east
    if ( $col < $#{$map->[0]}  &&  $map->[ $row ][ $col + 1 ] & WEST ) {
        $guess |= EAST
    }

    return $guess;
}

sub walkLoop {
    my ( $map, $row, $col, $step ) = @_;

    if ( $map->[$row][$col] & NORTH ) {
        $map->[$row][$col]     = ( $map->[$row][$col] & ~NORTH ) | VERTICAL_NORTH;
        $map->[$row - 1][$col] = ( $map->[$row - 1][$col] & ~SOUTH ) | VERTICAL_SOUTH;

        return walkLoop( $map, $row - 1, $col, $step + 1 );
    }

    if ( $map->[$row][$col] & EAST ) {
        $map->[$row][$col]     = ( $map->[$row][$col] & ~EAST ) | HORIZONTAL_EAST;
        $map->[$row][$col + 1] = ( $map->[$row][$col + 1] & ~WEST ) | HORIZONTAL_WEST;

        return walkLoop( $map, $row, $col + 1, $step + 1 );
    }

    if ( $map->[$row][$col] & SOUTH ) {
        $map->[$row][$col]     = ( $map->[$row][$col] & ~SOUTH ) | VERTICAL_SOUTH;
        $map->[$row + 1][$col] = ( $map->[$row + 1][$col] & ~NORTH ) | VERTICAL_NORTH;

        return walkLoop( $map, $row + 1, $col, $step + 1 );
    }

    if ( $map->[$row][$col] & WEST ) {
        $map->[$row][$col]     = ( $map->[$row][$col] & ~WEST ) | HORIZONTAL_WEST;
        $map->[$row][$col - 1] = ( $map->[$row][$col - 1] & ~EAST ) | HORIZONTAL_EAST;

        return walkLoop( $map, $row, $col - 1, $step + 1 );
    }

    return $step;
}


sub part2 {
    my ( $file ) = @_;

    my @map = loadCharacterMap( $file );

    my ( $startRow, $startCol );

    for( my $row=0; $row < scalar @map; $row++ ) {
        for( my $col=0; $col < scalar @{$map[$row]}; $col++ ) {
            if ( $map[ $row ][ $col ] eq 'S' ) {
                $startRow = $row;
                $startCol = $col;
            } else {
                $map[ $row ][ $col ] = $PIPE{ $map[ $row ][ $col ] } // 0;
            }
        }
    }

    $map[$startRow][$startCol] = guessMissingPipe( \@map, $startRow, $startCol ); 

    my $steps = walkLoop( \@map, $startRow, $startCol, 0 );

    my $inside = 0;
    my $count = 0;

    for( my $row=0; $row < scalar @map; $row++ ) {
        for( my $col=0; $col < scalar @{$map[$row]}; $col++ ) {
            if( $map[$row][$col] & VERTICAL ) {
                $count ^= ( $map[$row][$col] & VERTICAL );
            } else {
                if( $count == VERTICAL && ! ( $map[$row][$col] & HORIZONTAL ) ) {
                    $inside++;
                }
            }
        }
    }

    return $inside;
}


1;
