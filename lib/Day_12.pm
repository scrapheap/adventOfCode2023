package Day_12;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadList );
use List::Util qw( sum0 );

no warnings 'recursion';


sub part1 {
    my ( $file ) = @_;

    my @map = loadList( $file );

    return map { possibleArrangements( $_ ) } @map;
}


sub possibleArrangements {
    my ( $line, $damaged ) = map { split /\s+/ } @_;

    my @permutations = permute( $line, 0 );

    return grep { $damaged eq damagedList( $_ ) } @permutations;
}


sub permute {
    my ( $line, $i ) = @_;

    if ( $i > length( $line ) ) {
        return ( $line );
    }

    if ( substr( $line, $i, 1 ) eq "?" ) {
        substr( $line, $i, 1 ) = "#";
        my @permutations = permute( $line, $i + 1 );

        substr( $line, $i, 1 ) = ".";
        push @permutations, permute( $line, $i + 1 );

        return @permutations;
    }

    return permute( $line, $i + 1 );
}


sub damagedList{
    my ($line) = @_;

    my $damaged=0;
    my @list;
    my $l = length $line;

    for(my $i=0; $i<$l; $i++) {
        if ( substr( $line, $i, 1 ) eq "#" ) {
            $damaged++
        } else {
            if ( $damaged ) {
                push @list, $damaged;
                $damaged = 0;
            }
        }
    }

    if ( $damaged ) {
        push @list, $damaged;
    }

    return join(",", @list);
}


sub part2 {
    my ( $file ) = @_;

    my @map = loadList( $file );

    return sum0( map { permutations2( $_ ) } @map );
}

sub permutations2 {
    my ( $line, $damaged ) = map { split /\s+/ } @_;

    $line = join("?", $line, $line, $line, $line, $line );
    my @damaged = ( split /,/, $damaged ) x 5;

    return permute2( $line, 0, @damaged );
}

use Memoize;

memoize( 'permute2' );

sub permute2 {
    my ($str, $len, @groups) = @_;

    my $state = join( ",", $str, $len, @groups );

    my $ret = 0;

    if (!$str) {
        # Out of input, must decide if we found a match:
        # All groups accounted for, no hanging group.
        $ret = 1  if (@groups == 0 and $len == 0);

        # Check if hanging group is the size of the only remaining group:
        $ret = 1  if (@groups == 1 and $groups[0] == $len);

        return $ret;
    }

    if ( ! @groups ) {
        return ($str =~ m/^[^#]*$/);
    }

    # Advance one character:
    my $chr = substr( $str, 0, 1, '' );

    if ($chr ne '.') {   # ? or #
        # adv making grouping larger
        $ret += permute2( $str, $len + 1, @groups );
    }

    if ($chr ne '#') {   # ? or .
        if ($len == 0) {
            # no current grouping, just advance
            $ret += permute2( $str, 0, @groups );

        } elsif ($len == $groups[0]) {
            # current grouping matches current target
            shift @groups;
            $ret += permute2( $str, 0, @groups );
        }
    }

    return $ret;
}

1;
