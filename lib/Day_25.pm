package Day_25;

use v5.36;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadList );
use List::Util qw( shuffle reduce );

sub part1 {
    my ( $file ) = @_;

    my $connectionList = loadList( $file );

    my $graph = {};
    my $edges = {};

    foreach my $connection ( @$connectionList ) {
        my ($component1, $connectedWith) = $connection =~ m/^(\w+)\s*:\s*(.*)$/g;

        foreach my $component2 ( split /\s+/, $connectedWith ) {
            push @{ $graph->{ $component1 }}, { vertex => $component2, weight => 1 };
            push @{ $graph->{ $component2 }}, { vertex => $component1, weight => 1 };
            $edges->{join( ",", sort( $component1, $component2 ) ) } = 0;
        }
    }


    my @possibleComponents = keys %$graph;

    for ( 1..1000 ) {
        my $from = $possibleComponents[ rand scalar @possibleComponents ];
        my $to = $possibleComponents[ rand scalar @possibleComponents ];
        while ( $from eq $to ) {
            $to = $possibleComponents[ rand scalar @possibleComponents ];
        }

        my ( $distance, $path ) = dijkstraShortestPath( $graph, $from, $to );

        reduce { $edges->{join( ",", sort( $a, $b ) ) }++; $b } @$path;
    }

    my @mostUsedEdges = sort { $edges->{$b} <=> $edges->{$a} } keys %$edges;
    my %splitAt = map { $_ => 1 } @mostUsedEdges[0..2];

    my @left = group( $graph, \%splitAt );

    return scalar( @left ) * ( scalar( @possibleComponents ) - scalar( @left ) );
}


sub dijkstraShortestPath {
    my ( $graph, $source, $dest ) = @_;

    my @queue = ( [ $source, 0 ] );
    my $dist = {};
    my $prev = {};
    my $visited = {};

    $dist->{ $source } = 0;

    while( my ( $node, $weight ) = @{ shift @queue } ) {
        last if $node eq $dest;

        next if $visited->{ $node };

        $visited->{ $node } = 1;

        foreach my $edge ( @{ $graph->{ $node } } ) {
            if ( ! $visited->{ $edge->{vertex} } ) {
                if ( ! defined $dist->{ $edge->{vertex} }
                    || $dist->{$edge->{vertex}} > $dist->{ $node } + $edge->{weight}
                ) {
                    $dist->{$edge->{vertex}} = $dist->{ $node } + $edge->{weight};
                    $prev->{$edge->{vertex}} = $node;

                    push @queue, [ $edge->{vertex}, $dist->{$edge->{vertex}} ];
                }
            }
        }

        @queue = sort { $a->[1] <=> $b->[1] } @queue;
    }

    my @path = ( $dest );
    my $current = $dest;
    while ( $current = $prev->{ $current } ) {
        push @path, $current;
    }

    @path = reverse @path;

    return ( $dist->{ $dest }, \@path );
}


sub group {
    my ( $graph, $splitAt ) = @_;

    my ( $start ) = keys %$graph;

    my @queue = ( $start );
    my $seen = {};

    while ( my $node = pop @queue ) {
        next if $seen->{$node};
        $seen->{$node} = 1;

        foreach my $edge ( @{$graph->{$node}} ) {
            next if $splitAt->{ join( ",", sort ( $node, $edge->{vertex} ) ) };
            next if $seen->{ $edge->{vertex} };
            push @queue, $edge->{vertex};
        }
    }

    return keys %$seen;
}

sub part2 {
    my ( $file ) = @_;

}

1;
