package Day_14;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadCharacterMap  rotateMapClockwise  dumpMap );
use List::Util qw( sum0 min );


sub part1 {
    my ( $file ) = @_;

    my @platform = slopeNorth( loadCharacterMap( $file ) );

    return load( @platform );
}


sub slopeNorth {
    my @platform = @_;

    for( my $row = 0; $row < @platform; $row++ ) {
        for( my $col = 0; $col < @{ $platform[ $row ] }; $col++ ) {
            if ( $platform[$row][$col] eq '.' ) {
                my $southRock = findSouthRock( $row, $col, @platform );

                if ( $southRock ) {
                    $platform[$row][$col] = 'O';
                    $platform[$southRock][$col] = '.';
                }
            }
        }
    }

    return @platform;
}


sub load {
    my @platform = @_;

    my $total = 0;

    for( my $row = $#platform; $row >= 0; $row-- ) {
        my $rocks = grep { $_ eq 'O' } @{ $platform[ $row ] };

        $total += ( scalar @platform - $row ) * $rocks;
    }

    return $total;
}

sub findSouthRock {
    my ( $row, $col, @platform ) = @_;

    while ( $row < $#platform ) {
        $row++;
        if ( $platform[$row][$col] eq 'O' ) {
            return $row;
        } elsif ( $platform[$row][$col] eq '#' ) {
            last;
        }
    }

    return undef;
}

sub part2 {
    my ( $file ) = @_;

    my @platform = loadCharacterMap( $file );

    my %pastPlatform;

    my $cycle = 0;

    while( ! exists $pastPlatform{ fingerprint( @platform ) } ) {
        $pastPlatform{ fingerprint( @platform ) } = $cycle;
        $cycle++;
        @platform = cyclePlatform( @platform );
    }

    my $cycleLength = $cycle - $pastPlatform{ fingerprint( @platform ) };
    my $jumpAhead = $cycleLength * int( ( 1000000000 - $pastPlatform{ fingerprint( @platform ) } ) / $cycleLength );

    for( my $i = $pastPlatform{ fingerprint( @platform ) } + $jumpAhead; $i < 1000000000; $i++ ) {
        @platform = cyclePlatform( @platform );
    }

    return load( @platform );
}


sub fingerprint {
    return join("", map { @$_ } @_ );
}

sub cyclePlatform {
    my @platform = @_;
    
    for my $step ( 1..4 ) {
        @platform = slopeNorth( @platform );
        @platform = rotateMapClockwise( @platform );
    }

    return @platform;
}

1;
