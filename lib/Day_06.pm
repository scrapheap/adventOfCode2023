package Day_06;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadList );


sub part1 {
    my ( $file ) = @_;

    my @raceList = loadList( $file );
    my @races = parseRaces( @raceList );

    my $totalWaysToWin = 1;

    foreach my $race ( @races ) {
        my $waysToWin = 0;
        for( my $holdDownFor = 1; $holdDownFor < $race->{time}; $holdDownFor++ ) {
            my $distance = ( $race->{time} - $holdDownFor ) * $holdDownFor;
            if ( $distance > $race->{record} ) {
                $waysToWin++;
            }
        }

        $totalWaysToWin *= $waysToWin;
    }

    return $totalWaysToWin;
}


sub part2 {
    my ( $file ) = @_;

    my @raceList = map { $_ =~ s/\s*//gr } loadList( $file );
    my @races = parseRaces( @raceList );

    my $totalWaysToWin = 1;

    foreach my $race ( @races ) {
        my $waysToWin = 0;
        for( my $holdDownFor = 1; $holdDownFor < $race->{time}; $holdDownFor++ ) {
            my $distance = ( $race->{time} - $holdDownFor ) * $holdDownFor;
            if ( $distance > $race->{record} ) {
                $waysToWin++;
            }
        }

        $totalWaysToWin *= $waysToWin;
    }

    return $totalWaysToWin;
}


sub parseRaces {
    my @raceList = @_;

    my @times = $raceList[0] =~ m/(\d+)/g;
    my @records = $raceList[1] =~ m/(\d+)/g;

    my @races;

    while ( my $time = shift @times ) {
        my $record = shift @records;
        push @races, { time => $time, record => $record };
    }

    return @races;
}

1;
