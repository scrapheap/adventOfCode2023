package Day_20::Constants;

use v5.36;

use Exporter qw( import );

use constant  LOW => 0;
use constant HIGH => 1;

our @EXPORT_OK = qw( LOW HIGH );

1;
