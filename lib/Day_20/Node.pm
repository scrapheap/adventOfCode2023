package Day_20::Node;

use v5.36;

use Day_20::Constants qw( LOW HIGH );

use List::Util qw( all );

sub new {
    my ( $class, $name, $type ) = @_;

    my $self = {
        name => $name,
        type => $type // '',
        outputs => [],
        input => {},
        state => LOW,
    };

    return bless $self, $class;
}

sub type {
    my ( $self, $type ) = @_;

    $self->{type} = $type;
}

sub addInput {
    my ( $self, $input ) = @_;

    $self->{input}->{$input} = LOW;
}

sub setInput {
    my ( $self, $input, $value ) = @_;

    $self->{input}->{ $input } = $value;

    return $self->tick( $input );
}

sub addOutput {
    my ( $self, $output ) = @_;

    push @{$self->{outputs}}, $output;
}

sub tick {
    my ( $self, $input ) = @_;

    if ( $self->{type} eq "%" ) {
        if ( $self->{input}->{ $input } == LOW ) {
            $self->{state} ^= HIGH;
            return map { [ $self->{name}, $_, $self->{state} ] } @{ $self->{outputs} };
        } else {
            return ();
        }

    } elsif ( $self->{type} eq "&" ) {
        if ( all { $_ == HIGH } values %{ $self->{input} } ) {
            return map { [ $self->{name}, $_, LOW ] } @{ $self->{outputs} };

        } else {
            return map { [ $self->{name}, $_, HIGH ] } @{ $self->{outputs} };
        }

    } else {
        return map { [ $self->{name}, $_, $self->{input}->{ $input } ] } @{ $self->{outputs} };
    }
}

1;
