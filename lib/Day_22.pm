package Day_22;

use v5.36;
use feature 'multidimensional';

use Exporter qw( import );

our @EXPORT_OK = qw( part1 part2 );

use Utils qw( loadList );
use List::Util qw( min max );

sub part1 {
    my ( $file ) = @_;

    my $brickMap = loadList( $file );

    my @bricks;

    my $n = 1;
    foreach my $brickPos ( @$brickMap ) {
        my ( $x1, $y1, $z1, $x2, $y2, $z2 ) = $brickPos =~ m/(\d+)/g;

        my $brick={
            name => $n++,
            x => min( $x1, $x2 ),
            y => min( $y1, $y2 ),
            z => min( $z1, $z2 ),
            x2 => max( $x1, $x2 ),
            y2 => max( $y1, $y2 ),
            z2 => max( $z1, $z2 ),
        };

        push @bricks, $brick;
    }

    @bricks = sort { $a->{z} <=> $b->{z} } @bricks;

    $bricks[0]->{z2} = $bricks[0]->{z2} - ( $bricks[0]->{z} - 1 );
    $bricks[0]->{z} = 1;

    BRICK:
    for( my $b = 1; $b < @bricks; $b++ ) {
        my $brick = $bricks[ $b ];
        next BRICK if $brick->{z} == 1;

        while ( canDrop( $brick, @bricks[0..( $b - 1 )] ) ) {
            $brick->{z}--;
            $brick->{z2}--;
        }
    }

    @bricks = sort { $a->{z} <=> $b->{z} } @bricks;

    my @supporting;
    my @supportedBy;

    my $c = scalar @bricks;

    for( my $b = 0; $b < $c - 1; $b++ ) {
        for( my $ob = $b + 1; $ob < $c; $ob++ ) {
            if ( overlapsXY( $bricks[ $b ], $bricks[ $ob ] ) && $bricks[ $ob ]->{z} - $bricks[ $b ]->{z2} == 1 ) {
                push @{ $supporting[ $b ] }, $ob;
                push @{ $supportedBy[ $ob ] }, $b;
            }
        }
    }

    my $disintergrate = 0;
    for( my $b = 0; $b < $c; $b++ ) {
        my $canDisintergrate = 1;
        foreach my $ob ( @{ $supporting[ $b ] } ) {
            if ( scalar( @{ $supportedBy[ $ob ] } ) <= 1 ) {
                $canDisintergrate = 0;
            }
        }

        $disintergrate += $canDisintergrate;
    }

    return $disintergrate;
}

sub canDrop {
    my ( $brick, @bricks ) = @_;

    return 0 if $brick->{z} - 1 == 0;

    foreach my $otherBrick ( @bricks ) {
        if ( overlapsXY( $brick, $otherBrick ) && $otherBrick->{z2} >= $brick->{z} - 1 ) {
            return 0;
        }
    }
    return 1;
}

sub overlapsXY {
    my ($brick, $brick2 ) = @_;

    if (
        ( $brick->{x2} < $brick2->{x} || $brick->{x} > $brick2->{x2} )
        || ( $brick->{y2} < $brick2->{y} || $brick->{y} > $brick2->{y2} )
    ) {
        return 0;
    }

    return 1;
}

sub part2 {
    my ( $file ) = @_;

    my $brickMap = loadList( $file );

    my @bricks;

    my $n = 1;
    foreach my $brickPos ( @$brickMap ) {
        my ( $x1, $y1, $z1, $x2, $y2, $z2 ) = $brickPos =~ m/(\d+)/g;

        my $brick={
            name => $n++,
            x => min( $x1, $x2 ),
            y => min( $y1, $y2 ),
            z => min( $z1, $z2 ),
            x2 => max( $x1, $x2 ),
            y2 => max( $y1, $y2 ),
            z2 => max( $z1, $z2 ),
        };

        push @bricks, $brick;
    }

    @bricks = sort { $a->{z} <=> $b->{z} } @bricks;

    $bricks[0]->{z2} = $bricks[0]->{z2} - ( $bricks[0]->{z} - 1 );
    $bricks[0]->{z} = 1;

    BRICK:
    for( my $b = 1; $b < @bricks; $b++ ) {
        my $brick = $bricks[ $b ];
        next BRICK if $brick->{z} == 1;

        while ( canDrop( $brick, @bricks[0..( $b - 1 )] ) ) {
            $brick->{z}--;
            $brick->{z2}--;
        }
    }

    @bricks = sort { $a->{z} <=> $b->{z} } @bricks;

    my @supporting;
    my @supportedBy;

    my $c = scalar @bricks;


    for( my $b = 0; $b < $c - 1; $b++ ) {
        for( my $ob = $b + 1; $ob < $c; $ob++ ) {
            if ( overlapsXY( $bricks[ $b ], $bricks[ $ob ] ) && $bricks[ $ob ]->{z} - $bricks[ $b ]->{z2} == 1 ) {
                push @{ $supporting[ $b ] }, $ob;
                push @{ $supportedBy[ $ob ] }, $b;
            }
        }
    }

    my $wouldFall = 0;

    for( my $b = 0; $b < $c; $b++ ) {
        my $falling = { $b => 1 };
        my @toProcess = @{ $supporting[ $b ] // [] };

        FALLING_BRICK:
        while( my $nb = shift @toProcess ) {
            next FALLING_BRICK if $falling->{ $nb };
            foreach my $supportingBrick ( @{ $supportedBy[ $nb ] // [] } ) {
                next FALLING_BRICK unless $falling->{ $supportingBrick }; 
            }

            $falling->{ $nb } = 1;
            $wouldFall++;

            push @toProcess, @{ $supporting[ $nb ] // [] };
        }

    }

    return $wouldFall;
}

1;
