package Utils;

use Modern::Perl;
use Exporter qw( import );

our @EXPORT_OK = qw( loadList  loadCharacterMap  loadCharacterMaps  rotateMapClockwise dumpMap maxXY );

use Perl6::Slurp;
use Carp;

sub loadList {
    my ( $file ) = @_;

    croak "error reading file" unless -r $file;

    my @list = map { $_ =~ s/[\r\n]+$//r} slurp $file;

    return wantarray ? @list : \@list;
}


sub loadCharacterMap {
    my ( $file ) = @_;

    croak "error reading file" unless -r $file;

    return map { [ split '', $_ =~ s/[\r\n]+$//r ] } slurp $file;
}


sub loadCharacterMaps {
    my ( $file ) = @_;

    croak "error reading file" unless -r $file;

    my @multipleMaps = loadCharacterMap( $file );

    my @maps;
    my $map = [];

    while ( my $row = shift @multipleMaps ) {
        if ( @$row ) {
            push @$map, $row;
        } else {
            push @maps, $map;
            $map = [];
        }
    }
    
    push @maps, $map if @$map;

    return @maps;
}


sub rotateMapClockwise {
    my @map = @_;

    my @rotatedMap;

    for( my $i = 0; $i < @{$map[0]}; $i++) {
        push @rotatedMap, [ map { $_->[ $i ] } reverse @map ];
    }

    return @rotatedMap;
}


sub dumpMap {
    my @maps = @_;

    if ( 1 == scalar @maps && ref $maps[0] eq "ARRAY" ) {
        @maps = @{ $maps[0] };
    }

    warn join( "\n", map { join( '', @$_ ) } @maps ) . "\n";

    return;
}


sub maxXY {
    my @maps = @_;

    if ( 1 == scalar @maps && ref $maps[0] eq "ARRAY" ) {
        @maps = @{ $maps[0] };
    }

    return ( scalar( @{ $maps[0] } ), scalar( @maps ) );
}

1;
