use Modern::Perl;
use Test::More;
use Readonly;

use Day_01 qw( part1 part2 );

Readonly my $TEST_FILE => 'data/test/day_01';

subtest 'part 1' => sub {
    is( part1( $TEST_FILE ), 142, "part1 should return the correct answer" );
};


Readonly my $TEST_FILE_2 => 'data/test/day_01_2';

subtest 'part 2' => sub {
    is( part2( $TEST_FILE_2 ), 281, "part2 should return the correct answer" );
};


done_testing();
