use Modern::Perl;
use Test::More;
use Readonly;

use Day_11 qw( part1 part2 );

Readonly my $TEST_FILE => 'data/test/day_11';

subtest 'part 1' => sub {
    is( part1( $TEST_FILE ), 374, "part1 should return the correct answer" );
};


subtest 'part 2' => sub {
    $Day_11::expansion = 10;
    is( part2( $TEST_FILE ), 1030, "part2 should return the correct answer" );

    $Day_11::expansion = 100;
    is( part2( $TEST_FILE ), 8410, "part2 should return the correct answer" );
};


done_testing();
