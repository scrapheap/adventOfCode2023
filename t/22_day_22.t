use Modern::Perl;
use Test::More;
use Readonly;

use Day_22 qw( part1 part2 );

Readonly my $TEST_FILE => 'data/test/day_22';
Readonly my $TEST_FILE_2 => 'data/test/day_22_2';
Readonly my $TEST_FILE_3 => 'data/test/day_22_3';

subtest 'part 1' => sub {
    is( part1( $TEST_FILE ),   5, "part1 should return the correct answer" );
    is( part1( $TEST_FILE_2 ), 3, "part1 should return the correct answer" );
    is( part1( $TEST_FILE_3 ), 2, "part1 should return the correct answer" );
};

subtest 'part 2' => sub {
    is( part2( $TEST_FILE ),   7, "part2 should return the correct answer" );
};


done_testing();
