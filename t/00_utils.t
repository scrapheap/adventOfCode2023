use Modern::Perl;
use Test::More;
use Test::Exception;

use Utils qw( loadList  loadCharacterMap  loadCharacterMaps  rotateMapClockwise );

subtest 'loadList' => sub {
    throws_ok { loadList( 'data/non-existing' ) } qr/error reading file/,
        "If the file doesn't exist then we should throw an error";

    is_deeply(
        [ loadList( 'data/test/list.dat' ) ],
        [ 'line 1', 'line 2', 'line 3', 'line 4' ],
        "Each line in the file should be an entry in the array"
    );
};

subtest 'loadCharacterMap' => sub {
    throws_ok { loadCharacterMap( 'data/non-existing' ) } qr/error reading file/,
        "If the file doesn't exist then we should throw an error";

    is_deeply(
        [ loadCharacterMap( 'data/test/map.dat' ) ],
        [
            [ '#', '#', '#', '#', '#' ],
            [ '#', '#', ' ', ' ', '#' ],
            [ '#', ' ', '#', ' ', '#' ],
            [ '#', ' ', ' ', '#', '#' ],
            [ '#', '#', '#', '#', '#' ],
        ],
        "The loaded character map should be a 2D array of characters"
    );
};

subtest 'loadCharacterMaps' => sub {
    throws_ok { loadCharacterMap( 'data/non-existing' ) } qr/error reading file/,
        "If the file doesn't exist then we should throw an error";

    is_deeply(
        [ loadCharacterMaps( 'data/test/maps.dat' ) ],
        [
            [
                [ '#', '#', '#', '#', '#' ],
                [ '#', '#', ' ', ' ', '#' ],
                [ '#', ' ', '#', ' ', '#' ],
                [ '#', ' ', ' ', '#', '#' ],
                [ '#', '#', '#', '#', '#' ],
            ],
            [
                [ '#', '#', '#', '#', '#' ],
                [ '#', ' ', ' ', '#', '#' ],
                [ '#', ' ', '#', ' ', '#' ],
                [ '#', '#', ' ', ' ', '#' ],
                [ '#', '#', '#', '#', '#' ],
            ],
        ],
        "The loaded character map should be a 2D array of characters"
    );
};

subtest 'rotateMapClockwise' => sub {
    is_deeply(
        [
            rotateMapClockwise(
                [ '#', '#', '#', '#', '#' ],
                [ '#', '#', ' ', ' ', '#' ],
                [ '#', ' ', '#', ' ', '#' ],
                [ '#', ' ', ' ', '#', '#' ],
                [ '#', '#', '#', '#', '#' ],
            )
        ],
        [
            [ '#', '#', '#', '#', '#' ],
            [ '#', ' ', ' ', '#', '#' ],
            [ '#', ' ', '#', ' ', '#' ],
            [ '#', '#', ' ', ' ', '#' ],
            [ '#', '#', '#', '#', '#' ],
        ],
        "The map should have been rotated clockwise"
    );
};

done_testing();
