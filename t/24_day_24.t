use Modern::Perl;
use Test::More;
use Readonly;

use Day_24 qw( part1 part2 );

Readonly my $TEST_FILE => 'data/test/day_24';

subtest 'part 1' => sub {
    is( part1( $TEST_FILE, [ 7, 27 ] ),  2, "part1 should return the correct answer" );
};

subtest 'part 2' => sub {
    is( part2( $TEST_FILE ), 47, "part2 should return the correct answer" );
};


done_testing();
