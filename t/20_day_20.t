use Modern::Perl;
use Test::More;
use Readonly;

use Day_20 qw( part1 part2 );

Readonly my $TEST_FILE => 'data/test/day_20';
Readonly my $TEST_FILE_2 => 'data/test/day_20_2';

subtest 'part 1' => sub {
    is( part1( $TEST_FILE ), 32000000, "part1 should return the correct answer" );
    is( part1( $TEST_FILE_2 ), 11687500, "part1 should return the correct answer" );
};


done_testing();
