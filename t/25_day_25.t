use Modern::Perl;
use Test::More;
use Readonly;

use Day_25 qw( part1 part2 );

Readonly my $TEST_FILE => 'data/test/day_25';

subtest 'part 1' => sub {
    is( part1( $TEST_FILE ),  54, "part1 should return the correct answer" );
};

done_testing();
