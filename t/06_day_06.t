use Modern::Perl;
use Test::More;
use Readonly;

use Day_06 qw( part1 part2 );

Readonly my $TEST_FILE => 'data/test/day_06';

subtest 'part 1' => sub {
    is( part1( $TEST_FILE ), 288, "part1 should return the correct answer" );
};


subtest 'part 2' => sub {
    is( part2( $TEST_FILE ), 71503, "part2 should return the correct answer" );
};


done_testing();
